package netatmoapi

const (
	// DefaultBaseURL is netatmo api netatmoapi
	baseURL = "https://api.netatmo.com/"
	// DefaultAuthURL is netatmo auth netatmoapi
	AuthURL = baseURL + "oauth2/token"
	// DefaultDeviceURL is netatmo device netatmoapi
	getstationsdata = baseURL + "/api/getstationsdata"
	// DefaultGetDeviceURL is netatmo parse_identity netatmoapi
	GetDevices = baseURL + "/api/partnerdevices"
	//https://dev.netatmo.com/resources/technical/reference/common/getmeasure
	GetMeasure = baseURL + "/api/getmeasure"
)
