package netatmoapi

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

//Request_secure
func Request_secure(params url.Values, url, method string) []byte { //(error, []byte)
	//// Create request object.
	req, err := http.NewRequest(method, url, strings.NewReader(params.Encode()))
	// Set content type
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	// Set the header in the request.
	req.Header.Set("User-Agent", "Netatmo microservice/0.1")

	// Execute the request.
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(url + "?" + params.Encode())
	fmt.Println(string(body))
	//return err, body
	return body
}
