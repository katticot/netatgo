package netatmoapi

import (
	"encoding/json"
	"fmt"
	"net/url"
	"os"
)

func parseIdentity(body []byte) (*identity, error) {
	var s = new(identity)
	err := json.Unmarshal(body, &s)
	if err != nil {
		fmt.Println("whoops:", err)
	}
	return s, err
}

//identifiants

var (
	clientId     = os.Getenv("CLIENT_ID")
	clientSecret = os.Getenv("CLIENT_SECRET")
	username     = os.Getenv("NETATMO_USER_MAIL")
	password     = os.Getenv("NETATMO_USER_PASSWORD")
)

type identity struct {
	Access_token  string `json:"access_token"`
	Refresh_token string `json:"refresh_token"`
	Read_station  string `json:"read_station"`
}

func authent() (Access_token, Refresh_token string) {
	// création des paramètre
	params := url.Values{}
	params.Add("client_id", clientId)
	params.Add("client_secret", clientSecret)
	params.Add("username", username)
	params.Add("password", password)
	params.Add("grant_type", "password")

	body := Request_secure(params, AuthURL, "POST")

	s, _ := parseIdentity([]byte(body))

	println("Access_token : " + s.Access_token + "\n")
	println("Refresh_token : " + s.Refresh_token + "\n")
	return s.Access_token, s.Refresh_token

}
