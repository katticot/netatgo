package netatmoapi

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"
)

type deviceIds struct {
	//TODO changer variable  Fruit
	Fruit   []string `json:"body"`
	Created time.Time
}

func parseDeviceIds(body []byte) (*deviceIds, error) {
	var s = new(deviceIds)
	err := json.Unmarshal(body, &s)
	if err != nil {
		fmt.Println("Erreur parseDeviceIds:", err)
	}
	return s, err
}

func GetDevice() []string {

	// création des paramètre
	params := url.Values{}
	params.Add("access_token", authent())
	body := Request_secure(params, GetDevices, "POST")
	//fmt.Println(body)
	s, _ := parseDeviceIds([]byte(body))
	fmt.Println("\n")
	fmt.Println("récupération des ID " + s.Fruit[0] + "..." + s.Fruit[1] + "...")

	return s.Fruit

}
